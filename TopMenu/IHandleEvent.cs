namespace TopMenu
{
    public interface IHandleEvent
    {
        void HandleEvent(dynamic notification);
    }
}