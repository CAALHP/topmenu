using System;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Messaging;
using TopMenu.MessageTypes;

namespace TopMenu
{
    public class CaalhpEventHandler : IHandleEvent
    {
        private readonly TopMenuImplementation _topMenuImplementation;
        private readonly int _processId;

        public CaalhpEventHandler(TopMenuImplementation topMenuImplementation, int processId)
        {
            _topMenuImplementation = topMenuImplementation;
            _processId = processId;
        }

        public void HandleEvent(dynamic notification)
        {
            Console.WriteLine(notification);
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            Handle(obj);
        }

        private void Handle(UserLoggedInEvent user)
        {
            Messenger.Default.Send(new GenericMessage<UserLoggedInEvent>(_topMenuImplementation, user));
        }

        private void Handle(UserLoggedOutEvent user)
        {
            if(_processId != user.CallerProcessId)
                Messenger.Default.Send(new ExternalLogoutMessage());
        }

        private void Handle(ScanFingerRequestEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<ScanFingerRequestEvent>(_topMenuImplementation, obj));
        }

        private void Handle(ScanFingerResponseEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<ScanFingerResponseEvent>(_topMenuImplementation, obj));
        }

        private void Handle(HideTopMenuEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<HideTopMenuEvent>(_topMenuImplementation, obj));
        }

        private void Handle(ShowTopMenuEvent obj)
        {
            Messenger.Default.Send(new GenericMessage<ShowTopMenuEvent>(_topMenuImplementation, obj));
        }

    }
}