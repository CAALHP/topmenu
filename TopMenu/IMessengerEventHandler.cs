namespace TopMenu
{
    public interface IMessengerEventHandler
    {
        void RegisterEvents();
    }
}