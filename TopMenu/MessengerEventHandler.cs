﻿using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using GalaSoft.MvvmLight.Messaging;
using TopMenu.MessageTypes;

namespace TopMenu
{
    public class MessengerEventHandler : IMessengerEventHandler
    {
        private readonly TopMenuImplementation _topMenuImplementation;
        private readonly IHostCAALHPContract _host;

        public MessengerEventHandler(TopMenuImplementation topMenuImplementation, caalhp.Core.Contracts.IHostCAALHPContract hostCAALHPContract)
        {
            _topMenuImplementation = topMenuImplementation;
            _host = hostCAALHPContract;
        }

        public void RegisterEvents()
        {
            Messenger.Default.Register<ShowHomeMessage>(_topMenuImplementation, HandleShowHomeApp);
            Messenger.Default.Register<LogoutMessage>(_topMenuImplementation, HandleUserLoggedOut);
            Messenger.Default.Register<CloseCaalhpMessage>(_topMenuImplementation, HandleCloseCaalhp);
            Messenger.Default.Register<UpdateUsersMessage>(_topMenuImplementation, HandleUpdateUsers);
        }

        private void HandleUpdateUsers(UpdateUsersMessage obj)
        {
            var command = new GenericInfoEvent
            {
                Info = "UpdateUsers",
                CallerName = "TopMenu",
                //CallerProcessId = _topMenuImplementation._processId,
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.ReportEvent(serializedCommand);
        }

        private void HandleCloseCaalhp(CloseCaalhpMessage obj)
        {
            var command = new GenericInfoEvent
            {
                Info = "CloseCAALHP",
                CallerName = "TopMenu",
                //CallerProcessId = _topMenuImplementation._processId,
            };
            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.ReportEvent(serializedCommand);
        }

        private void HandleUserLoggedOut(LogoutMessage obj)
        {
            var command = new UserLoggedOutEvent()
            {
                CallerName = "TopMenu",
                //CallerProcessId = _topMenuImplementation._processId,
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.ReportEvent(serializedCommand);
        }

        private void HandleShowHomeApp(ShowHomeMessage obj)
        {
            var command = new ShowAppEvent
            {
                CallerName = "TopMenu",
                //CallerProcessId = _topMenuImplementation._processId,
                AppName = "HomeScreen",
            };

            var serializedCommand = EventHelper.CreateEvent(SerializationType.Json, command);

            _host.ReportEvent(serializedCommand);

        }
    }
}