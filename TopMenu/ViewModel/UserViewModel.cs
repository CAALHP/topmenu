﻿using GalaSoft.MvvmLight;

namespace TopMenu.ViewModel
{
    public class UserViewModel : ViewModelBase
    {
        private string _userImage;
        private string _userName;
        private string _userLevel;


        public string UserImage
        {
            get { return _userImage; }
            set
            {
                _userImage = value;
                RaisePropertyChanged(() => UserImage);
            }
        }

        public string UserName
        {
            get { return _userName; }
            set
            {
                _userName = value;
                RaisePropertyChanged(() => UserName);
            }
        }

        public string UserLevel
        {
            get { return _userLevel; }
            set
            {
                _userLevel = value;
                RaisePropertyChanged(() => UserLevel);
            }
        }
    }
}
