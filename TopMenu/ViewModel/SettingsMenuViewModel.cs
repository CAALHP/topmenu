﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.TopMenu;
using TopMenu.MessageTypes;
using TopMenu.Properties;

namespace TopMenu.ViewModel
{
    public class SettingsMenuViewModel : ViewModelBase
    {

        private ObservableCollection<SettingsButtonViewModel> _settingsButtonList;
        private bool _isSettingsEnabled;

        public SettingsMenuViewModel()
        {
            _settingsButtonList = new ObservableCollection<SettingsButtonViewModel>();
            try
            {
                //get default culture
                var culture = CaalhpConfigHelper.GetDefaultCulture();
                //set language
                if (string.IsNullOrWhiteSpace(culture)) 
                    culture = "de-DE";
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            IsSettingsEnabled = false;
            
            var logoutCommand = new RelayCommand(LogOutCommand);
            var logoutButton = new SettingsButtonViewModel( SettingsButtonType.User, Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", logoutCommand)
                {
                    ButtonText = Resources.ResourceManager.GetString("SignOut")
                };

            SettingsButtonList.Add(logoutButton);
            SetupDeveloperMode();
            Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, HandleUserLoggedInEvent);
            Messenger.Default.Register<ExternalLogoutMessage>(this, HandlerExternalUserLoggedOut);
        }

        private void SetupDeveloperMode()
        {
            var developerMode = Convert.ToBoolean(ConfigurationManager.AppSettings.Get("DeveloperMode"));
            if (!developerMode) return;
            AddCloseButton();
            AddUpdateUsersButton();
        }

        private void AddUpdateUsersButton()
        {
            var updateUsersCommand = new RelayCommand(UpdateUsersCommand);
            var updateUsersButton = new SettingsButtonViewModel(SettingsButtonType.Common,
                Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", updateUsersCommand)
            {
                ButtonText = Resources.ResourceManager.GetString("UpdateUsers"),
                IsEnabled = true
            };
            SettingsButtonList.Add(updateUsersButton);
        }

        private void UpdateUsersCommand()
        {
            Messenger.Default.Send(new UpdateUsersMessage());
        }

        private void AddCloseButton()
        {
            var closeCaalhpCommand = new RelayCommand(CloseCaalhpCommand);
            var closeCaalhpButton = new SettingsButtonViewModel(SettingsButtonType.Common,
                Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", closeCaalhpCommand)
            {
                ButtonText = Resources.ResourceManager.GetString("Close"),
                IsEnabled = true
            };
            SettingsButtonList.Add(closeCaalhpButton);
        }

        private void CloseCaalhpCommand()
        {
            //Send Close CAALHP message
            Messenger.Default.Send(new CloseCaalhpMessage());
        }

        private void HandlerExternalUserLoggedOut(ExternalLogoutMessage obj)
        {
            EnableUserButtons(false);
        }

        private void HandleUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        {
            EnableUserButtons(true);
        }

        public bool IsSettingsEnabled
        {
            get { return _isSettingsEnabled; }
            set
            {
                _isSettingsEnabled = value;
                RaisePropertyChanged(() => IsSettingsEnabled);
            }
        }

        public ObservableCollection<SettingsButtonViewModel> SettingsButtonList
        {
            get { return _settingsButtonList; }
            set
            {
                _settingsButtonList = value;
                RaisePropertyChanged(() => SettingsButtonList);
            }
        }

        private void LogOutCommand()
        {

            EnableUserButtons(false);

            Messenger.Default.Send(new LogoutMessage());
            IsSettingsEnabled = false;
        }

        private void EnableUserButtons(bool value)
        {
            foreach (var settingsButtonViewModel in SettingsButtonList.Where(settingsButtonViewModel => settingsButtonViewModel.Type == SettingsButtonType.User))
            {
                settingsButtonViewModel.IsEnabled = value;
            }
        }

    }
}
