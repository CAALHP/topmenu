﻿using System;
using System.Threading.Tasks;
using System.Windows;
using caalhp.Core.Events;
using caalhp.Core.Events.Types;
using caalhp.IcePluginAdapters;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Plugins.TopMenu;
using TopMenu.MessageTypes;

namespace TopMenu.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private BaseButtonViewModel _homeButton;
        private BaseButtonViewModel _settingsButton;
        private UserViewModel _user;
        private SettingsMenuViewModel _settingsMenu;

        private ScanFingerViewModel _scanFinger;
        private bool _shouldScanFinger;

        private AppAdapter _adapter;
        private TopMenuImplementation _imp;
        private Visibility _topMenuVisibility;
        
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            Task.Run(() =>
            {
                const string endpoint = "localhost";
                try
                {
                    _imp = new TopMenuImplementation();
                    _adapter = new AppAdapter(endpoint, _imp);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
            });
            var homeCommand = new RelayCommand(ShowHomeClickCommandExecute);
            HomeButton = new TopButtonViewModel(Utils.AssemblyDirectory + "\\Images\\homeIcon.png", "#FFFFFFFF", "#FF000000", homeCommand);

            var showSettingsCommand = new RelayCommand(ShowSettingsCommand);
            SettingsButton = new TopButtonViewModel(Utils.AssemblyDirectory + "\\Images\\menuIcon.png", "#FF3DBFD9", "#FF000000", showSettingsCommand);

            SettingsMenu = new SettingsMenuViewModel();
            ShouldScanFinger = false;
            TopMenuVisibility = Visibility.Visible;
            //ScanFinger = new ScanFingerViewModel();

            //HomeButton = new HomeButtonViewModel(@"Images\homeIcon.png", "#FF000000", "#FFFFFFFF");
            Messenger.Default.Register<GenericMessage<UserLoggedInEvent>>(this, HandleUserLoggedInEvent);
            Messenger.Default.Register<GenericMessage<ScanFingerRequestEvent>>(this, HandleScanFingerEvent);
            Messenger.Default.Register<GenericMessage<ScanFingerResponseEvent>>(this, HandleFingerScannedEvent);
            Messenger.Default.Register<GenericMessage<HideTopMenuEvent>>(this, HandleHideTopMenu);
            Messenger.Default.Register<GenericMessage<ShowTopMenuEvent>>(this, HandleShowTopMenu);
            Messenger.Default.Register<LogoutMessage>(this, HandlerUserLoggedOut);
            Messenger.Default.Register<ExternalLogoutMessage>(this, HandlerUserLoggedOut);

            //_timer = new Timer(HandleScanFingerEvent, true, 4000, 4000);
        }

        private void HandleHideTopMenu(GenericMessage<HideTopMenuEvent> obj)
        {
            //System.Media.SystemSounds.Asterisk.Play();
            TopMenuVisibility = Visibility.Hidden;
        }

        private void HandleShowTopMenu(GenericMessage<ShowTopMenuEvent> obj)
        {
            TopMenuVisibility = Visibility.Visible;
        }

        private void HandleFingerScannedEvent(GenericMessage<ScanFingerResponseEvent> obj)
        {
            ShouldScanFinger = false;
        }

        private void HandleScanFingerEvent(GenericMessage<ScanFingerRequestEvent> obj)
        {
            if (ScanFinger == null)
                ScanFinger = new ScanFingerViewModel();
            ShouldScanFinger = true;
        }


        private void HandlerUserLoggedOut(LogoutMessage obj)
        {
            if (IsUserLogged)
                User = null;
        }

        private void HandlerUserLoggedOut(ExternalLogoutMessage obj)
        {
            if (IsUserLogged)
                User = null;
        }

        private void ShowSettingsCommand()
        {
            SettingsMenu.IsSettingsEnabled = !SettingsMenu.IsSettingsEnabled;
        }

        private void HandleUserLoggedInEvent(GenericMessage<UserLoggedInEvent> obj)
        {
            var user = obj.Content.User;
            //var sourcePath = obj.Content.PhotoRoot;

            try
            {
                var imgPath = Utils.GetUserImage(obj.Content.PhotoRoot);

                User = new UserViewModel()
                {
                    UserName = user.FirstName,
                    UserLevel = Enum.GetName(typeof(UserRole), user.Role), //At somepoint something for this would be nice.
                    UserImage = imgPath
                };

            }
            catch (Exception e)
            {
                User = null;
                throw new Exception(e.Message);
            }
        }

        public BaseButtonViewModel HomeButton
        {
            get { return _homeButton; }
            set
            {
                _homeButton = value;
                RaisePropertyChanged(() => HomeButton);
            }
        }

        public BaseButtonViewModel SettingsButton
        {
            get { return _settingsButton; }
            set
            {
                _settingsButton = value;
                RaisePropertyChanged(() => SettingsButton);
            }
        }

        public SettingsMenuViewModel SettingsMenu
        {
            get { return _settingsMenu; }
            set
            {
                _settingsMenu = value;
                RaisePropertyChanged(() => SettingsMenu);
            }
        }

        public UserViewModel User
        {
            get { return _user; }
            set
            {
                _user = value;
                RaisePropertyChanged(() => User);
                RaisePropertyChanged(() => IsUserLogged);
            }
        }

        public bool IsUserLogged
        {
            get { return _user != null; }
        }

        public ScanFingerViewModel ScanFinger
        {
            get { return _scanFinger; }
            set
            {
                _scanFinger = value;
                RaisePropertyChanged();
            }
        }

        public bool ShouldScanFinger
        {
            get { return _shouldScanFinger; }
            set
            {
                _shouldScanFinger = value;
                RaisePropertyChanged();
            }
        }

        public Visibility TopMenuVisibility
        {
            get { return _topMenuVisibility; }
            set
            {
                _topMenuVisibility = value;
                RaisePropertyChanged();
            }
        }

        public void ShowHomeClickCommandExecute()
        {
            Messenger.Default.Send(new ShowHomeMessage());
        }

        public void Show()
        {

        }

    }
}