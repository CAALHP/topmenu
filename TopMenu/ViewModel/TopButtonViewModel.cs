﻿using GalaSoft.MvvmLight.Command;

namespace TopMenu.ViewModel
{
    public class TopButtonViewModel: BaseButtonViewModel
    {
        public TopButtonViewModel(string iconPath, string backGroundColour, string highLightColour, RelayCommand clickCommand)
            : base(iconPath, backGroundColour, highLightColour, clickCommand)
        {
           // iconPath = "\Images\homeIcon.png";
        }

        //public override void ClickCommandExecute()
        //{
        //    Messenger.Default.Send(new ShowHomeMessage()); 
        //}
    }
}
