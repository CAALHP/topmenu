﻿using GalaSoft.MvvmLight.Command;

namespace TopMenu.ViewModel
{
    public enum SettingsButtonType
    {
        Common,
        User
    }

    public class SettingsButtonViewModel : BaseButtonViewModel
    {
        private string _buttonText;
        private bool _isEnabled;


        public SettingsButtonViewModel(SettingsButtonType type, string iconPath, string backGroundColour, string highLightColour, RelayCommand clickCommand)
            : base(iconPath, backGroundColour, highLightColour, clickCommand)
        {

            Type = type;
        }

        //public int Height
        //{
        //    get { return _height; }
        //    set
        //    {
        //        _height = value;
        //        RaisePropertyChanged(() => Height);
        //    }
        //}

        public string ButtonText
        {
            get { return _buttonText; }
            set
            {
                _buttonText = value;
                RaisePropertyChanged(() => ButtonText);
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                RaisePropertyChanged(() => IsEnabled);
            }
        }

        public SettingsButtonType Type { get; set; }



        //public override void ClickCommandExecute()
        //{
        //    Messenger.Default.Send(new NotificationMessage<SettingsButtonViewModel>(this, "ClickCommand")); 
        //}
    }
}
