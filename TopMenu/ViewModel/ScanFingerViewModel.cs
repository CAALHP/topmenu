﻿using System;
using System.Globalization;
using System.Threading;
using System.Windows;
using caalhp.Core.Utils.Helpers;
using GalaSoft.MvvmLight;
using TopMenu.Properties;

namespace TopMenu.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ScanFingerViewModel : ViewModelBase
    {
        private string _scanFingerText;

        public string ScanFingerText
        {
            get { return _scanFingerText; }
            set
            {
                _scanFingerText = value;
                RaisePropertyChanged();
            }
        }
        /// <summary>
        /// Initializes a new instance of the ScanFingerViewModel class.
        /// </summary>
        public ScanFingerViewModel()
        {
            try
            {
                //get default culture
                var culture = CaalhpConfigHelper.GetDefaultCulture();
                //set language
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            ScanFingerText = Resources.ResourceManager.GetString("ScanFingerText");
        }
    }
}