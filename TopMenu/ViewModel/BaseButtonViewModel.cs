﻿using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;

namespace TopMenu.ViewModel
{
    public class BaseButtonViewModel : ViewModelBase
    {
        private string _iconPath;
        private string _backGroundColour;
        private string _highLightColour;

        protected BaseButtonViewModel(string iconPath, string backGroundColour, string highLightColour, RelayCommand clickCommand = null)
        {
            IconPath = iconPath;
            BackgroundColour = backGroundColour;
            HighLightColour = highLightColour;
            ClickCommand = clickCommand ?? new RelayCommand(ClickCommandExecute);
        }

        public string IconPath
        {
            get { return _iconPath; }
            private set
            {
                _iconPath = value;
                RaisePropertyChanged();
            }
        }

        public string BackgroundColour
        {
            get
            {
                return _backGroundColour;
            }
            private set
            {
                _backGroundColour = value;
                RaisePropertyChanged();
            }
        }

        public string HighLightColour
        {
            get
            {
                return _highLightColour;
            } 
            private set
            {
                _highLightColour = value;
                RaisePropertyChanged();
            }
        }

        #region Command

        public ICommand ClickCommand { get; private set; }

        public virtual void ClickCommandExecute()
        {
            // nothing will happen unless its overloaded :D    
        }

        #endregion
    }
}
