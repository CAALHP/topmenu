﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Plugins.TopMenu
{
    public static class Utils
    {
        public static string AssemblyDirectory
        {
            get
            {
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                var uri = new UriBuilder(codeBase);
                var path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public static string GetUserImage(string photoLink, string sourcePath)
        {
            var imagePath =  AssemblyDirectory + "\\Images\\";

            if (string.IsNullOrEmpty(photoLink))
                return imagePath + "Default.png";

            try
            {
                var targetImgPath = Path.Combine(imagePath, photoLink);
                var sourceImgPath = Path.Combine(sourcePath, photoLink);

                CopyImagefromSource(sourceImgPath, targetImgPath);

                return targetImgPath; //image = Image.FromFile(targetImgPath);
            }
            catch (Exception)
            {
                return imagePath + "Default.png";
            }
        }

        /*private async Task LoadImage()
        {
            await Task.Run(() => DownloadIcon(_appItem.Icon));
        }*/

        public static string GetUserImage(string url)
        {
            var imageFolder = AssemblyDirectory + "\\Images\\";
            var web = new WebClient();
            var fileName = Path.GetFileName(url);
            if (fileName == null) return imageFolder + "Default.png";
            var imagePath = Path.Combine(imageFolder, fileName);
            try
            {
                if (!File.Exists(imagePath))
                    web.DownloadFile(url, Path.Combine(imageFolder, imagePath));
                return imagePath;
            }
            catch (Exception e)
            {
                return imageFolder + "Default.png";
            }
        }

        private static void CopyImagefromSource(string sourceImgPath, string targetImgPath)
        {
            if (!File.Exists(targetImgPath) ||
                File.GetLastWriteTime(targetImgPath) < File.GetLastWriteTime(sourceImgPath))
            {
                File.Copy(sourceImgPath, targetImgPath, true);
            }

        }
    }
}
