﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Threading;
using caalhp.Core.Contracts;
using caalhp.Core.Events;
using caalhp.Core.Utils.Helpers;
using caalhp.Core.Utils.Helpers.Serialization;
using TopMenu.ViewModel;

namespace TopMenu
{
    public class TopMenuImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private readonly MainViewModel _main;
        private IHandleEvent _caalhpEventHandler;
        private MessengerEventHandler _messengerEventHandler;
        //private Thread _thread;

        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        //private volatile Application _app;

        public TopMenuImplementation()
        {
            var vml = Application.Current.Resources["Locator"] as ViewModelLocator;
            if (vml == null) return;
            _main = vml.Main;
            
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _caalhpEventHandler = new CaalhpEventHandler(this, _processId);
            Subscribe(_host.Host);
            _messengerEventHandler = new MessengerEventHandler(this, hostObj.Host);
            _messengerEventHandler.RegisterEvents();
        }

        private void Subscribe(IHostCAALHPContract publisher)
        {
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof (UserLoggedInEvent)), _processId);
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof (UserLoggedOutEvent)), _processId);
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof (ScanFingerRequestEvent)), _processId);
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof (ScanFingerResponseEvent)), _processId);
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof(HideTopMenuEvent)), _processId);
            publisher.SubscribeToEvents(GetNameSpaceFromEvent(typeof(ShowTopMenuEvent)), _processId);
        }

        private static string GetNameSpaceFromEvent(Type type)
        {
            return EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, type);
        }

        /// <summary>
        /// deserialize notification and dispatch to handler dynamically at runtime
        /// </summary>
        /// <param name="notification"></param>
        public void Notify(KeyValuePair<string, string> notification)
        {
            _caalhpEventHandler.HandleEvent(notification);
        }

        public string GetName()
        {
            return "TopMenu";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Application.Current.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Show()
        {
            DispatchToApp(() => _main.Show());
        }

        private static void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }
    }
}