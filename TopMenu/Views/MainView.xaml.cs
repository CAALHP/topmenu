﻿using System.Timers;
using System.Windows;
using GalaSoft.MvvmLight.Threading;

namespace TopMenu.Views
{
    /// <summary>
    /// Description for MainView.
    /// </summary>
    public partial class MainView
    {
        private Timer _timer;
        
        /// <summary>
        /// Initializes a new instance of the MainView class.
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            DispatcherHelper.Initialize();
            _timer = new Timer(100);
            _timer.AutoReset = true;
            _timer.Elapsed += _timer_Elapsed;
            _timer.Enabled = true;
            _timer.Start();
            //Focusable = false;
            //Application.Current.Deactivated += Current_Deactivated;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(BringToFront);
        }

        private void BringToFront()
        {
            Topmost = false;
            Topmost = true;
        }
    }
}