﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace CAALPHUILib
{
    /// <summary>
    /// Interaction logic for MainWindowTitle.xaml
    /// </summary>
    public partial class MainWindowTitle : UserControl
    {

        public MainWindowTitle()
        {
            this.InitializeComponent();

            DataContext = this;

            //Text="{Binding Path=DataContext.MainTitle }"

        }

        static FrameworkPropertyMetadata propertymetadata =
            new FrameworkPropertyMetadata("Default",
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault |
            FrameworkPropertyMetadataOptions.Journal, new
            PropertyChangedCallback(MainTitle_PropertyChanged),
            new CoerceValueCallback(MainTitle_CoerceValue),
            false, UpdateSourceTrigger.PropertyChanged);

        public static readonly DependencyProperty MainTitleProperty = DependencyProperty.Register("MainTitle", typeof(String), typeof(MainWindowTitle), propertymetadata);

        private static void MainTitle_PropertyChanged(DependencyObject dobj, DependencyPropertyChangedEventArgs e)
        {
            //To be called whenever the DP is changed.
            //MessageBox.Show(string.Format(
            // "Property changed is fired : OldValue {0} NewValue : {1}", e.OldValue, e.NewValue));

            //  MessageBox.Show(dobj.GetType().ToString());

        }

        private static object MainTitle_CoerceValue(DependencyObject dobj, object Value)
        {
            //called whenever dependency property value is reevaluated. The return value is the
            //latest value set to the dependency property
            //MessageBox.Show(string.Format("CoerceValue is fired : Value {0}", Value));
            return Value;
        }

        public String MainTitle
        {
            get
            {
                return (String)this.GetValue(MainTitleProperty);
            }
            set
            {
                //this.header.Text = value; 

                this.SetValue(MainTitleProperty, value);

            }
        }


        public static void SetMainTitle(DependencyObject dependencyObject, String value)
        {
            dependencyObject.SetValue(MainTitleProperty, value);
            // MessageBox.Show(string.Format("DataValidation is Fired : Value {0}", value));
        }

        public static String GetMainTitle(DependencyObject dependencyObject)
        {
            return (String)dependencyObject.GetValue(MainTitleProperty);
        }







    }
}